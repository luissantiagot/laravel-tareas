<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TareaController;
use App\Http\Controllers\CategoriaController;

Route::get('tareas', [TareaController::class, 'index']);
Route::get('categorias', [CategoriaController::class, 'index']);

Route::group(['prefix' => 'tarea'], function () {
    Route::post('add', [TareaController::class, 'add']);
    Route::post('update/{id}', [TareaController::class, 'update']);
    Route::delete('delete/{id}', [TareaController::class, 'delete']);
});

