<?php

use App\Http\Controllers\TareaController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
     return abort(404);
});

Route::get('/tareas', function () {
    return view('tareas');
});

Route::group(['prefix' => 'tarea'], function () {
    Route::get('/', [TareaController::class, 'index']);
    Route::post('add', [TareaController::class, 'add']);
    Route::post('update/{id}', [TareaController::class, 'update']);
    Route::delete('delete/{id}', [TareaController::class, 'delete']);
});
