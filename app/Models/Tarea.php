<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    use HasFactory;
    protected $fillable = ['nombre'];
	public $timestamps = true;
	
    public function categoria(){
        return $this->belongsToMany('App\Models\Categoria', 'categorias_tarea');
    }


}
