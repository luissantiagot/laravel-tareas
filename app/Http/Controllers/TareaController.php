<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tarea;

class TareaController extends Controller
{

    public function index()
    {
        /* Si se quiere sacar con 1 consulta
         select t.id,t.nombre as nombre_tarea,c.nombre as nombre_categoria from `tareas` t
            inner join categorias_tarea ct on ct.tarea_id = t.id
            inner join categorias c on c.id = ct.categoria_id
			
			o Tarea::select()->join()->join();
			...
        */
        $tarea = Tarea::with('Categoria')
            ->orderBy('id', 'desc')
			->get();
			// ->keyBy('nombre');
		return $tarea;

    }
	
    public function add(Request $request)
    {

		if ($request->missing('nombre')) {
			return response()->json('No se ha escrito nombre para la tarea');
		}
		
        $tarea = new Tarea([
            'nombre' => $request->input('nombre'),
        ]);

		$tarea->save();
		
		if ($request->has('categoria')) {
			foreach( $request->input('categoria') as $cat => $valor){
				if($valor){
					$tarea->categoria()->attach($cat);	
					$tarea->save();				
				}
			}
		}
		
        return response()->json([ 'id' => $tarea->id , 'message' => 'Tarea creada']);
    }

    public function edit(int $id)
    {
        $tarea = Tarea::with('Categoria')
            ->find($id)
            ->toArray();

        return response()->json($tarea);
    }

    public function update(int $id, Request $request)
    {
        $tarea = Tarea::find($id);
        $tarea->update($request->all());

        return response()->json('Tarea actualizada');
    }

    public function delete(int $id)
    {
        $tarea = Tarea::find($id);
        $tarea->delete();

        return response()->json('Tarea borrada');
    }
}
