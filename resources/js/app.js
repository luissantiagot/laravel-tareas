require('./bootstrap');
import Vue from 'vue';
window.Vue = require('vue');

import App from './components/Tareas.vue';
import VueAxios from 'vue-axios';
import axios from 'axios';

Vue.use(VueAxios, axios);

const app = new Vue({
    el: '#app',
    render: h => h(App),
});
