import Tareas from "./components/Tareas";
import EditarTarea from "./components/EditarTarea";

export const routes = [
    {
        name: 'Tareas',
        path: '/',
        component: Tareas
    },
    {
        name: 'Editar',
        path: 'edit/:id',
        component: EditarTarea
    }
];
