<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tarea;
use App\Models\Categoria;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    protected $categorias = ['PHP', 'Javacript', 'CSS'];

    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('categorias')->truncate();
        DB::table('tareas')->truncate();
        DB::table('categorias_tarea')->truncate();

        foreach ($this->categorias as $c){
            $categoria = Categoria::insert([
                'nombre' => $c,
            ]);
        }

        for($i=0; $i<10; $i++){
            $tarea = Tarea::insert([
                'nombre' => 'Tarea '.$i,
            ]);
			$tarea = Tarea::latest('id')->first();
            DB::table('categorias_tarea')->insert(
				[ 
					'categoria_id' => rand(1,3),
					'tarea_id' => $tarea->id
				]
            );

        }







    }
}
