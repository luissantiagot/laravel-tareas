# Laravel 8 : Tareas #

- [x] Creada API Rest
- [X] Creado Controlador Tarea
- [X] Creados schemas de la base de datos (relacional)
- [X] Creado seeder de ejemplo 
- [X] Creadas rutas api 

## llamadas api ###

* /api/tareas : Devuelve todas las tareas
	* Devuelve todas las tareas y sus categorías
	* Método GET
* /api/tareas/add
	* Crea una tarea
	* Método POST 
* /api/tareas/delete/{id}
	* Borra una tarea
	* Método DELETE
	
### TODO ###
* /api/update/{id}
	* Actualiza una tarea
	* Método post